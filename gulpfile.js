'use strict';

var gulp = require('gulp'),
    gulpSequence = require('gulp-sequence'),
    jade = require('gulp-jade'),
    sass = require('gulp-sass'),
    wiredep = require('wiredep').stream,
    inject = require('gulp-inject'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    browserSync = require("browser-sync"),
    clean = require('gulp-clean'),
    reload = browserSync.reload,
    notify = require('gulp-notify'),   
    rename = require("gulp-rename"),
    iconfont = require("gulp-iconfont"),
    iconfontCss = require("gulp-iconfont-css"),
    svgsprite = require("gulp-svg-sprite"),
    cheerio = require("gulp-cheerio"),
    replace = require("gulp-replace"),
    svgmin = require("gulp-svgmin"),
    ftp = require('vinyl-ftp'),
    smartgrid = require('smart-grid'),
    wait = require("gulp-wait"),
    jadeInheritance = require('gulp-jade-inheritance'),    
    changed = require('gulp-changed'),
    cached = require('gulp-cached'),    
    filter = require('gulp-filter');


var path = {
    ftp: {
        host: '85.143.174.31',
        user: 'aidar',
        password: 'B7u1S4c4',
        folderProject: 'anna'
    },
    build: {
        folder: 'build',
        html: 'build/html/',
        js: 'build/scripts/',
        css: 'build/css/',
        img: 'build/images/',
        fonts: 'build/fonts/'
    },
    serve: {
        html: 'src/html/',        
        css: 'src/css/'
    },    
    src: {
        folder: 'src',
        html: 'src/jade/**/*.jade',
        js: 'src/scripts/**/*.js',
        style: 'src/sass/**/*.*',
        img: 'src/images/**/**',
        fonts: 'src/fonts/**/*.*'
    },   

    watch: {
        html: 'src/jade/**/*.jade',
        js: 'src/scripts/**/*.js',
        style: 'src/sass/**/*.*',
        img: 'src/images/**/**',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: ["./", path.src.folder, path.build.folder]
    },
    host: 'localhost',
    port: 9000,
    tunnel: true
};

gulp.task('deploy', function(){
    var conn = ftp.create({ 
        host: path.ftp.host, 
        user: path.ftp.user, 
        password: path.ftp.password,
        parallel: 10 
    });
    return gulp.src(path.build.folder + '/**', { base: './build/', buffer: false })
        .pipe(conn.newer('/pro-7.ru/' + path.ftp.folderProject)) // only upload newer files
        .pipe(conn.dest('/pro-7.ru/' + path.ftp.folderProject));
});


gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function () {
    return gulp.src(path.clean, {read: false})
        .pipe(clean());
});

gulp.task('smartgrid', function () {
    
    var settings = {
        filename: '_smart-grid',
        outputStyle: 'sass', /* less || scss || sass || styl */
        columns: 12, /* number of grid columns */
        // offset: 0, /* gutter width px || % */
        mobileFirst: false,
        container: {
            maxWidth: '1600px'
            // fields: '30px'
        },
        breakPoints: {            
            lg: {
                width: '1100px', /* -> @media (max-width: 1100px) */
            },
            md: {
                width: '960px'
            },
            sm: {
                width: '780px',
                fields: '15px'
            },
            xsm: {
                width: '650px'               
            },
            xs: {
                width: '560px'
            },
            xxs: {
                width: '480px'
            }
            /*
            some_name: {
                width: 'Npx',
                fields: 'N(px|%|rem)',
                offset: 'N(px|%|rem)'
            }
            */
        }
    };
    smartgrid('src/sass/basic/', settings);
});


gulp.task('jade', function () {
    gulp.src(path.src.html)
        //only pass unchanged *main* files and *all* the partials
        .pipe(changed(path.serve.html, {
            extension: '.html'
        }))
        //filter out unchanged partials, but it only works when watching
        .pipe(gulpif(global.isWatching, cached('jade')))
        //find files that depend on the files that have changed
        .pipe(jadeInheritance({
            basedir: 'src/jade/'
        }))
        // filter out partials (folders and files starting with "_" )
        .pipe(filter(function (file) {
            return !/\/_/.test(file.path) && !/^_/.test(file.relative);
        }))
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest(path.serve.html))
        .pipe(reload({stream: true}));
});

gulp.task('inject', function () {
    return gulp.src('src/**/*.html')
        .pipe(useref())
        // .pipe(gulpif('*.js', uglify().on('error', gutil.log)))
        .pipe(gulpif('*.css', cssmin()))
        .pipe(gulp.dest(path.build.folder))
        .pipe(replace('css/', '../../css/'))
        .pipe(replace('scripts/', '../../scripts/'))
        .pipe(gulp.dest(path.build.folder))
});


gulp.task('styles', function () {
    gulp.src(path.src.style)
        .pipe(wait(300))
        .pipe(sass({ errLogToConsole: true }).on('error', 
            notify.onError({
            message: "Error: <%= error.message %>",
            title  : "Styles!"
        } ) ))
        .pipe(prefixer())
        .pipe(gulp.dest(path.serve.css))
        .pipe(reload({stream: true}));
});

gulp.task('wiredep', function(){
  gulp.src('./src/jade/template/template.jade')
    .pipe(wiredep({
        directory: './vendor'
        // exclude: ['jquery']
    }))
    .pipe(inject(gulp.src('./src/scripts/libs/**/*.js', { read: false }), {
        relative: true,
        name: 'inject-not-bower'
    }))
    .pipe(inject(gulp.src('./src/scripts/*.js', { read: false }), {
        relative: true
    }))   
    .pipe(gulp.dest('./src/jade/template/'));
});


gulp.task('image:build',function(){
    gulp.src(path.src.img) 
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.img));
});

gulp.task('svg:build', function () {
    gulp.src('src/images/svg/*.svg')
        .pipe(svgmin({
            js2svg: {
                pretty: true
            }
        }))
        .pipe(cheerio({
            run: function ($) {
                $('[fill]').removeAttr('fill');
                $('[stroke]').removeAttr('stroke');
                $('[style]').removeAttr('style');
            },
            parserOptions: { xmlMode: true }
        }))
        .pipe(replace('&gt;', '>'))
        .pipe(svgsprite({
            mode: {
                symbol: {
                    sprite: "sprite.svg"
                }
            }
        }))
        .pipe(gulp.dest('src/images/svg/'));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('iconfont', function(){
    gulp.src(['src/images/svg-iconfont/**/*.svg'])
        .pipe(iconfontCss({
            fontName: 'iconic',
            path: 'src/sass/fonts/_iconfont-template.scss',
            targetPath: '../sass/fonts/_iconfont.scss',
            fontPath: '../fonts/',
            cssClass: 'iconic'
        }))
        .pipe(iconfont({
            fontName: 'iconic',
            formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
            startUnicode: true,
            prependUnicode: true,
            normalize: true,
            fontHeight: 1001,
            centerHorizontally: true
        }))
        .pipe(
            gulp.dest('src/fonts')
        );
});

gulp.task('index-list', function() {
    return gulp
        .src(path.src.folder +'index.html')
        .pipe(
            gulp.dest(path.build.folder)
        );
});


gulp.task('watch', function () {
    watch([path.watch.html], function(event, cb) {
        gulp.start('setWatch');
        gulp.start('jade');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('styles');
    });
    watch([path.watch.js], function(event, cb){
        browserSync.reload();
    });
    watch('bower.json', function(event, cb) {
        gulp.start('wiredep');
    });
});

gulp.task('setWatch', function () {
    global.isWatching = true;
});

gulp.task('serve', [
    'wiredep',
    'jade',
    'styles',
    'webserver',
    'watch'
]);

gulp.task( 'build', gulpSequence(['index-list','wiredep','jade','styles','inject','fonts:build','image:build']) );

gulp.task('default', ['serve']);