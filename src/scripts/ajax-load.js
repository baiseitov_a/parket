$(function () {

  $('.js-load-btn').on("click", function (e) {
    e.preventDefault();
    var self = $(this);
    var container = self.parents('.js-load-block').find('.js-load-container');
    var href = self.attr('href');

    $.get(
      href, {},
      function (data) {
        if ($(data).length != 0) {
          container.append($(data));
          $('.work-slider').twentytwenty({
            before_label: 'До',
            after_label: 'После'
          })
        } else {
          self.hide();
        }
      });
  });

})