window.viewportUnitsBuggyfill.init();

function isWidth(width, flag) {
  if (flag) {
    if ($W.width() < width) return true
    else return false
  } else {
    if ($W.width() > width) return true
    else return false
  }
}

$(document).ready(function () {

  if ($('.work-slider').length) {
    $('.work-slider').twentytwenty({
      before_label: 'До',
      after_label: 'После'
    })
  }

  $W.scroll(function(){
    var height = $('.header-inner').outerHeight();
    if ($W.scrollTop() > height){
      $('.header').addClass('fixed')
    }
    else{
      $('.header').removeClass('fixed')
    }
  })

});

$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(function(){

  $(window).on('scroll', function () {
    if ( $('.counter').length ) {
      if ($('.counter').isInViewport()) {
        counterNumber();
      }
    }
  });

  if ($('.counter').length) {
    if ($('.counter').isInViewport()) {
      counterNumber();
    }
  }
  
  $('.js-visible').viewportChecker();


  function counterNumber(){
    $('.counter').each(function () {
      var $this = $(this),
        countTo = $this.attr('data-count');
      $({
        countNum: $this.text()
      }).animate({
          countNum: countTo
        },
  
        {
          duration: 800,
          easing: 'linear',
          step: function () {
            $this.text(Math.floor(this.countNum));
          },
          complete: function () {
            $this.text(this.countNum);
          }
        });
    });
  }



  $('#nav-icon1').click(function () {
    $(this).toggleClass('open');
    $('.header-nav__list').slideToggle();
  });

  $('.header-nav__dropdown').click(function () { 
    $(this).toggleClass('show');
    $(this).find('ul').toggle();
  });
  
})


$(function(){

  function numberWithSpaces(x){
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
  }
  
  $('.js-calculate').click(function(e){
    e.preventDefault();
    var tariff = $('.js-tariff option:selected').attr('value');
    var build = $('.js-build').val();
    if( build <= 20 ) build = 20;
    if (tariff == 'clear' || build == ''){
      $('.calc-error').show();
      $('.calc-result').hide();
    }
    else{
      $('.calc-error').hide();
      $('.calc-result').show();      
      $('.calc-result__value').html( numberWithSpaces(tariff * build) );
    }

  })

})