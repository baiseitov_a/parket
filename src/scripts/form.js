$(function(){

  /****** ***Ввод в инпут только цифр ***********/
  
  $('.js-number').keypress(function(e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  })
  
  /****** ****Ввод в инпут только букв ***********/
  $('.js-letter').inputmask('Regex',
    { regex: '^[а-яА-Яa-zA-Z ]*$' }
  );

  /*********** Маска для телефона **************/

    $('.js-phone').inputmask("+7 (999) 999-9999");

    $('select').styler();

})