$(function () {
  var $popup = $('.js-popup');
  var $popupOpenBtn = $('.js-popup-btn');
  var $popupCloseBtn = $('.js-popup-close');

  $popupOpenBtn.click(function () {    
    $(this).toggleClass('active');
    var id = $(this).attr('href');
    $popup.not(id).removeClass('popup-show');
    $(id).toggleClass('popup-show');
    return false
  })

  $popupCloseBtn.click(function () {
    $(this).parents('.popup').removeClass('popup-show');
    $popupOpenBtn.removeClass('active');   
          
  })

  
  $('.popup-youtube').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });
  

})