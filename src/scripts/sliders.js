$(function () {

  /* promo slider */

  $('.js-promo-slider').slick({
    arrows: true,
    prevArrow: "<button type='button' class='slick-prev pull-left'></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'></button>",
    speed: 800,
    autoplay: true,
    autoplaySpeed: 2000,
    dots: true,
    fade: true    
  })

  /* promo slider */

  $('.js-letters-slider').not('.slick-initialized').slick({
    arrows: true,
    prevArrow: "<button type='button' class='slick-prev pull-left'></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'></button>",
    slidesToShow: 3,    
    speed: 800,
    dots: true,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })

  /* service slider */

  $('.js-services-slider').slick({
    arrows: true,
    prevArrow: "<button type='button' class='slick-prev pull-left'></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'></button>",        
    speed: 800,
    dots: true
  })

  $('.js-services-block__slider').slick({
    arrows: true,
    slidesToShow: 4, 
    prevArrow: "<button type='button' class='slick-prev pull-left'></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'></button>",        
    speed: 800,
    dots: true,
    responsive: [
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  })

})